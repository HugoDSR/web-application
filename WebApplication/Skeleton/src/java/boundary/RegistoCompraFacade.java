/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.RegistoCompra;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tiagoteixeira
 */

@Stateless
public class RegistoCompraFacade extends AbstractFacade<RegistoCompra> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(RegistoCompra registo) {
        em.persist(registo);
    }
    
    public RegistoCompraFacade() {
        super(RegistoCompra.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
