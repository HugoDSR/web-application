/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Funcionario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tiagoteixeira
 */
@Stateless
public class FuncionarioFacade extends AbstractFacade<Funcionario> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(Funcionario func) {
        em.persist(func);
    }
    
    public FuncionarioFacade() {
        super(Funcionario.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
