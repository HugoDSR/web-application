/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.ProdutoTemplate;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


@Stateless
public class ProdutoTemplateFacade extends AbstractFacade<ProdutoTemplate> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(ProdutoTemplate produto) {
        em.persist(produto);
    }

    public List<ProdutoTemplate> getAllGuests() {
        TypedQuery<ProdutoTemplate> query = em.createQuery("SELECT g FROM ProdutoTemplate g ORDER BY g.id", ProdutoTemplate.class);
        return query.getResultList();
    }


    public List<ProdutoTemplate> getProdutoNovidade() {
        TypedQuery<ProdutoTemplate> query = em.createQuery("SELECT g FROM ProdutoTemplate g WHERE g.novidade='true'", ProdutoTemplate.class);
        return query.getResultList();
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProdutoTemplateFacade() {
        super(ProdutoTemplate.class);
    }

}
