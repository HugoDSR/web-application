/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.CarrinhoCompras;
import entities.Item;
import entities.ProdutoTemplate;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class CarrinhoComprasFacade extends AbstractFacade<CarrinhoCompras> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(CarrinhoCompras carrinho) {
        em.persist(carrinho);
    }

    public List<CarrinhoCompras> getAllGuests() {
        TypedQuery<CarrinhoCompras> query = em.createQuery("SELECT g FROM CARRINHO_COMPRAS g ORDER BY g.id", CarrinhoCompras.class);
        return query.getResultList();
    }

    /*public List<ProdutoTemplate> getProdutosCarrinho(int id) {
     TypedQuery<CarrinhoCompras> query1 = em.createQuery("SELECT g FROM COMPRA g WHERE g.id=:id", CarrinhoCompras.class);
     query1.setParameter("id", id);
     TypedQuery<ProdutoTemplate> query2 = em.createQuery("SELECT g FROM CARRINHO_COMPRAS g WHERE g.id=:id", ProdutoTemplate.class);

     return query.getResultList();
     }*/
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarrinhoComprasFacade() {
        super(CarrinhoCompras.class);
    }

    public List<Item> item(int idProduto, String size) {
        TypedQuery<Item> query = em.createQuery("SELECT g FROM Item g WHERE g.idTemplate=:idProduto and g.tamanho=:size", Item.class);
        query.setParameter("idProduto", idProduto);
        query.setParameter("size", size);
        return query.getResultList();
    }
    
    

}
