/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Produto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ProdutoFacade extends AbstractFacade<Produto> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(Produto produto) {
        em.persist(produto);
    }

    public List<Produto> getAllGuests() {
        TypedQuery<Produto> query = em.createQuery("SELECT g FROM Produto g ORDER BY g.id", Produto.class);
        return query.getResultList();
    }

    public ProdutoFacade() {
        super(Produto.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
