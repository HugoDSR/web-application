/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author TheBeardedMan
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(Cliente cliente) {
        em.persist(cliente);
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    
    public Cliente find(String mail, String pass) {
        try{
            TypedQuery<Cliente> query1 = em.createQuery("SELECT g FROM Cliente g WHERE g.email=:mail and g.password=:pass", Cliente.class);
            query1.setParameter("mail", mail);
            query1.setParameter("pass", pass);
            return query1.getSingleResult();
        }
        catch(NoResultException  ex){
            return null;
        }
    }

    public List<Cliente> getAllGuests() {
        TypedQuery<Cliente> query = em.createQuery("SELECT g FROM Cliente g ORDER BY g.id", Cliente.class);
        return query.getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
