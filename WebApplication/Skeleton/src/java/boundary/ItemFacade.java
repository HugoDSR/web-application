/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Item;
import entities.ProdutoTemplate;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ItemFacade extends AbstractFacade<Item> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    @Override
    public void create(Item entity) {
        super.create(entity);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ItemFacade() {
        super(Item.class);
    }

    public List<Item> getAllGuests() {
        TypedQuery<Item> query = em.createQuery("SELECT g FROM Item g ORDER BY g.id", Item.class);
        return query.getResultList();
    }
      
    public List<Item> tamanhosDisponiveis(ProdutoTemplate id_template) {
        TypedQuery<Item> query = em.createQuery("SELECT DISTINCT g.tamanho FROM Item g WHERE g.idTemplate=:id_template", Item.class);
        query.setParameter("id_template", id_template);
        return query.getResultList();
    } 

    
}