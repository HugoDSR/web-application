/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Pagamento;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tiagoteixeira
 */

@Stateless
public class PagamentoFacade extends AbstractFacade<Pagamento> {

    @PersistenceContext(unitName = "iesPU2")
    private EntityManager em;

    public void persist(Pagamento pagamento) {
        em.persist(pagamento);
    }
    
    public PagamentoFacade() {
        super(Pagamento.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
