package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 *
 *
 * @author rui
 *
 */
@Entity

@Table(name = "PRODUTO")

public class Produto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    String descricao;
    double preco;
    String tipo;
    String estilo;
    String codigo;
    boolean novidade;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public Produto() {
        //construtor vazio.
    }

    public void setNovidade(boolean novidade) {
        this.novidade = novidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }
    
    public void setPreco(double preco) {
        this.preco = preco;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public String getCodigo() {

        return codigo;

    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isNovidade() {
        return novidade;
    }
}
