/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CARRINHO_COMPRAS")
public class CarrinhoCompras {
    
    @Id
    @GeneratedValue
    private int id;
    
    private double precoTotal;
    
    @OneToOne(fetch=FetchType.LAZY, mappedBy="carrinho")
    private RegistoCompra registo;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(double precoTotal) {
        this.precoTotal = precoTotal;
    }
    
    @ManyToMany
    @JoinTable(name = "COMPRA", joinColumns = @JoinColumn(name="idCarrinho"),inverseJoinColumns = @JoinColumn(name="idTemplate"))
    private List<ProdutoTemplate> produtos;

    public RegistoCompra getRegisto() {
        return registo;
    }

    public void setRegisto(RegistoCompra registo) {
        this.registo = registo;
    }

    public List<ProdutoTemplate> getProduto() {
        return produtos;
    }

    public void setProduto(List<ProdutoTemplate> produto) {
        this.produtos = produto;
    }
    
    
}
