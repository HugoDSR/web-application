package entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "FUNCIONARIO")
public class Funcionario {

    @Id
    @GeneratedValue
    private int id;
    
    private String user;
    private String password;
    
    @OneToMany(mappedBy="funcionario")
    private List<RegistoCompra> registo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<RegistoCompra> getRegisto() {
        return registo;
    }

    public void setRegisto(List<RegistoCompra> registo) {
        this.registo = registo;
    }
    
    public void addRegisto(RegistoCompra registo) {
        this.registo.add(registo);
        if(registo.getFuncionario() != this) {
            registo.setFuncionario(this);
        }
    }
}
