/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ITEM")
public class Item {
    
    @Id
    @GeneratedValue
    private int id;
    
    private String qrcode;
    private int barcode;
    private String tamanho;
    
    @ManyToOne(fetch=FetchType.LAZY)
    private ProdutoTemplate idTemplate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public ProdutoTemplate getIdTemplate() {
        return idTemplate;
    }

    public void setIdTemplate(ProdutoTemplate idTemplate) {
        this.idTemplate = idTemplate;
        if(!idTemplate.getItems().contains(this)) {
            idTemplate.getItems().add(this);
        }
    }
    
    
}
