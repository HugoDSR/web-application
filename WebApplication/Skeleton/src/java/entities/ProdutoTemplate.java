/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author rui
 */
@Entity
@Table(name = "PRODUTO_TEMPLATE")
public class ProdutoTemplate {

    @Id
    @GeneratedValue
    private int id;

    private String descricao;
    private double preco;
    private String tipo;
    private String estilo;
    private boolean novidade;
    private String url;
    

    @OneToMany(mappedBy="idTemplate")
    private List<Item> items;
    
    @ManyToMany(mappedBy="produtos")
    private List<CarrinhoCompras> carrinho;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public boolean isNovidade() {
        return novidade;
    }

    public void setNovidade(boolean novidade) {
        this.novidade = novidade;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
    
    public void addItems(Item item) {
        this.items.add(item);
        if(item.getIdTemplate() != this) {
            item.setIdTemplate(this);
        }
    }

    public List<CarrinhoCompras> getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(List<CarrinhoCompras> carrinho) {
        this.carrinho = carrinho;
    }
    
}
