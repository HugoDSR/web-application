/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REGISTO_COMPRA")
public class RegistoCompra {

    @Id
    @GeneratedValue
    private int id;
    
    private String data;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="idCarrinho")
    private CarrinhoCompras carrinho;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="idPagamento")
    private Pagamento pagamento;
    
    @ManyToOne(fetch=FetchType.LAZY)
    private Cliente cliente;
    
    @ManyToOne(fetch=FetchType.LAZY)
    private Funcionario funcionario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public CarrinhoCompras getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(CarrinhoCompras carrinho) {
        this.carrinho = carrinho;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
        if(!cliente.getRegisto().contains(this)) {
            cliente.getRegisto().add(this);
        }
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
        if(!funcionario.getRegisto().contains(this)) {
            funcionario.getRegisto().add(this);
        }
    }
    
    
}
