/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
public class Cliente {

    @Id
    @GeneratedValue
    private int id;
    
    private String nome;
    private String morada;
    private String email;
    private int telemovel;
    private int nContribuinte;
    private String password;
    
    @OneToMany(mappedBy="cliente")
    private List<RegistoCompra> registo;
    
    public String getPassword() {
        return password;
    }

    public List<RegistoCompra> getRegisto() {
        return registo;
    }

    public void setRegisto(List<RegistoCompra> registo) {
        this.registo = registo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelemovel(int telemovel) {
        this.telemovel = telemovel;
    }

    public void setnContribuinte(int nContribuinte) {
        this.nContribuinte = nContribuinte;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getMorada() {
        return morada;
    }

    public String getEmail() {
        return email;
    }

    public int getTelemovel() {
        return telemovel;
    }

    public int getnContribuinte() {
        return nContribuinte;
    }
    
    public void addRegisto(RegistoCompra registo) {
        this.registo.add(registo);
        if(registo.getCliente() != this) {
            registo.setCliente(this);
        }
    }
    
}
