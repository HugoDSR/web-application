/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import boundary.ClienteFacade;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author hugo
 */

@WebService(name = "BeardedMan", portName = "BeardedManport", serviceName = "BeardedManService"
,targetNamespace = "http://www.BeardedMan.com")
public class Soapimpl {
    @EJB
    private ClienteFacade clienteFacade;
    
    @WebMethod(operationName = "validLogin")
    public String validLogin(@WebParam(name="user") String user, @WebParam(name="pass") String pass){
        FacesMessage message = null;
        if(this.clienteFacade.find(user, pass) != null){
            return "caixa";
        }
        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
        return "index";
    }
    
    
    
}
