/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.FuncionarioFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tiagoteixeira
 */

@ManagedBean(name = "FuncionarioView")
@RequestScoped
public class FuncionarioView {
    
    @EJB
    private FuncionarioFacade funcionarioFacade;

    public FuncionarioView() {
        
    }
}
