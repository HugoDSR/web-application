/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ClienteFacade;
import entities.Cliente;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tiagoteixeira
 */

@ManagedBean(name = "ClienteView")
@RequestScoped
public class ClienteView {
    
    @EJB
    private ClienteFacade clienteFacade;
    
    private final Cliente cliente;

    public ClienteView() {
        this.cliente = new Cliente();
    }
    
    public String validLogin(){
        FacesMessage message = null;
        if(this.clienteFacade.find(this.cliente.getEmail(), this.cliente.getPassword()) != null){
            return "login_session";
        }
        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
        return "registar";
    }
    
    public Cliente getCliente(){
        return this.cliente;
    }

    public List getAllProducts() {
        return clienteFacade.getAllGuests();
    }
    
    public String addCliente(){
        clienteFacade.persist(this.cliente);
        return "login";
    }
    
    public String logout(){
        return "index";
    }
    
    public void newCliente(){
        
        System.out.println(" -- inserting new product"); //EXEMPLO
        Cliente p = new Cliente();
        p.setNome("Tiago Teixeira");
        p.setMorada("Rua das Meninas");
        p.setEmail("tiagoteixeira@ua.pt");
        p.setTelemovel(912976757);
        p.setnContribuinte(25250505);
        p.setPassword("qwerty");
        clienteFacade.persist(p);
        
                Cliente p2 = new Cliente();
        p2.setNome("Miguel Antunes");
        p2.setMorada("Rua dos Meninos");
        p2.setEmail("miguelantunes@ua.pt");
        p2.setTelemovel(912976758);
        p2.setnContribuinte(25250507);
        p2.setPassword("qwerty");
        clienteFacade.persist(p2);
        
                Cliente p3 = new Cliente();
        p3.setNome("Hugo Rocha");
        p3.setMorada("Rua dos menininhos");
        p3.setEmail("hugorocha@ua.pt");
        p3.setTelemovel(912973757);
        p3.setnContribuinte(25150505);
        p3.setPassword("qwerty");
        clienteFacade.persist(p3);
        
                Cliente p4 = new Cliente();
        p4.setNome("Sergio Martins");
        p4.setMorada("Rua das menininhas");
        p4.setEmail("sergiomartins@ua.pt");
        p4.setTelemovel(912976711);
        p4.setnContribuinte(25251105);
        p4.setPassword("qwerty");
        clienteFacade.persist(p4);
        
        //return "3";
    }
    
}
