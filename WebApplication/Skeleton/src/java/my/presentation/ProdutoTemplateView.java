/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ProdutoFacade;
import boundary.ProdutoTemplateFacade;
import entities.Item;
import entities.Produto;
import entities.ProdutoTemplate;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tiagoteixeira
 */

@ManagedBean(name = "ProdutoTemplateView")
@RequestScoped
public class ProdutoTemplateView {
    
    @EJB
    private ProdutoTemplateFacade produtoFacade;
    
    private ProdutoTemplate produtoTemplate;
    
    private List<ProdutoTemplate> produtos;

    public ProdutoTemplateView() {
        produtoTemplate = new ProdutoTemplate();
    }

    public ProdutoTemplate getProdutoTemplate() {
        return produtoTemplate;
    }

    public void setProdutoTemplate(ProdutoTemplate produtoTemplate) {
        this.produtoTemplate = produtoTemplate;
    }
    
    @PostConstruct
    public void init() {
        produtos = produtoFacade.getAllGuests();
    }
    

    
    public String getDescricao(){
        return this.produtoTemplate.getDescricao();
    }
 
    public List<ProdutoTemplate> getProdutos(){
        return produtos;
    }
    public List getAllProducts() {
        return produtoFacade.getAllGuests();
    }
    
    /*public void newProduto(){
        
        System.out.println(" -- inserting new product"); //EXEMPLO
        Produto p = new Produto();
        p.setDescricao("teste");
        p.setPreco(23.3);
        p.setTipo("Camisa");
        p.setEstilo("A");
        p.setCodigo("QWERETY234");
        p.setNovidade(true);
        produtoFacade.persist(p);
        
        //return "3";
    }*/
    
}
