/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ItemFacade;
import entities.Item;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tiagoteixeira
 */

@ManagedBean(name = "ItemView")
@RequestScoped
public class ItemView {
    
    @EJB
    private ItemFacade itemFacade;
    
    private Item item;

    public ItemView() {
        this.item =  new Item();
    }
    
    public Item getItem(){
        return this.item;
    }
    public List getAllItens() {
        return itemFacade.getAllGuests();
    }
}
