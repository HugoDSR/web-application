/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.CarrinhoComprasFacade;
import boundary.CompraFacade;
import boundary.ItemFacade;
import boundary.ProdutoTemplateFacade;
import boundary.RegistoCompraFacade;
import entities.CarrinhoCompras;
import entities.Compra;
import entities.Item;
import entities.ProdutoTemplate;
import entities.RegistoCompra;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tiagoteixeira
 */
@ManagedBean(name = "RegistoCompraView")
@RequestScoped
public class RegistoCompraView {

    //     Entreprise Bean //
    @EJB
    private ItemFacade itemFacade;
    @EJB
    private ProdutoTemplateFacade produtoTemplateFacade;
    @EJB
    private RegistoCompraFacade registoFacade;
    @EJB
    private CarrinhoComprasFacade carrinhoComprasFacade;
    @EJB
    private CompraFacade compraFacade;

    private ProdutoTemplate produtoTemplate;
    private Item item;
    private RegistoCompra registo;
    private CarrinhoCompras carrinhoCompras;
    private Compra compra;

    public final List<ProdutoTemplate> listProd;
    private int id;
    private String size;

    public RegistoCompraView() {
        this.item = new Item();
        this.produtoTemplate = new ProdutoTemplate();
        this.registo = new RegistoCompra();
        this.carrinhoCompras = new CarrinhoCompras();
        this.compra = new Compra();
        listProd = new ArrayList<>();
    }

    public List<ProdutoTemplate> getListProd() {
        produtoTemplate = getProduto(id);
        this.listProd.add(produtoTemplate);
        return this.listProd;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getName() {
        produtoTemplate = getProduto(id);
        return produtoTemplate.getDescricao();
    }

    public String getUrl() {
        produtoTemplate = getProduto(id);
        return produtoTemplate.getUrl();
    }

    public Double getPrice() {
        produtoTemplate = getProduto(id);
        return produtoTemplate.getPreco();
    }

    public String getTipo() {
        produtoTemplate = getProduto(id);
        return produtoTemplate.getTipo();
    }

    public void setCompra() {
        int i = this.carrinhoCompras.getId();
 //       this.compra.getId(i);
        this.compra.getIdProduct();
    }

    public void setCarrinhoCompras() {
        this.carrinhoCompras.getId();
    }

    public ProdutoTemplate getProdById() {
        return produtoTemplateFacade.find(id);
    }

    public void addToList(ProdutoTemplate produto) {
        listProd.add(produto);
    }

    public String creatRegisto() {

        registo.setCarrinho(carrinhoCompras);
        registoFacade.persist(registo);
        return "carrinho_compras";
    }

    public ProdutoTemplate getProduto(int id) {
        return produtoTemplateFacade.find(id);
    }

}
