/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ItemFacade;
import boundary.ProdutoTemplateFacade;
import entities.Item;
import entities.ProdutoDescricao;
import entities.ProdutoTemplate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author hugo
 */
@ManagedBean
@RequestScoped
public class ProdutoDescricaoView {
    @EJB
    private ItemFacade itemFacade;
    @EJB
    private ProdutoTemplateFacade produtoTemplateFacade;
    
    private List<Item> tamanho;
    private String tamanhoP;
    
    private List<ProdutoTemplate> produto;


    private int id;

    private ProdutoTemplate produtoTemplate;
    
    public ProdutoDescricaoView() {
        produto = new ArrayList<>();
    }    
    
    public List<ProdutoTemplate> getListInstance(){
        return produto;
    }
    
    @PostConstruct
    public void init() {
        produto = produtoTemplateFacade.getAllGuests();
    }

    public List<ProdutoTemplate> getProduto() {
        return produto;
    }

    public void setProduto(List<ProdutoTemplate> produto) {
        this.produto = produto;
    }

    public ProdutoTemplate getProdutoTemplate(){
        return produtoTemplate;
    }
    public int getId() {
        return id;
    }

    public String getTamanhoP(){
        return tamanhoP;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    
    public String getName(){
        produtoTemplate = getProduto(id);
        return produtoTemplate.getDescricao();
    }
    
    public String getUrl(){
        produtoTemplate = getProduto(id);
        return produtoTemplate.getUrl();
    }
    
    public Double getPrice(){
        produtoTemplate = getProduto(id);
        return produtoTemplate.getPreco();
    }
    
    public String getTipo(){
        produtoTemplate = getProduto(id);
        return produtoTemplate.getTipo();
    }
    
    public List<Item> getSize(){
        tamanho = itemFacade.tamanhosDisponiveis(produtoTemplate);
        return tamanho;
    }

    public ProdutoTemplate getProduto(int id){
        return produtoTemplateFacade.find(id);
    }
    
    public String butonAction(){
        
        RegistoCompraView registoCompraView = new RegistoCompraView();
        registoCompraView.addToList(getProduto(id));
        return "carrinho_compras";
    }
}
  
                       
                    