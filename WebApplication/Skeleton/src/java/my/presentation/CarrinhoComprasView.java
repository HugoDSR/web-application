/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.CarrinhoComprasFacade;
import boundary.ItemFacade;
import boundary.ProdutoTemplateFacade;
import entities.Item;
import entities.ProdutoDescricao;
import entities.ProdutoTemplate;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tiagoteixeira
 */

@ManagedBean(name = "CarrinhoComprasView")
@RequestScoped
public class CarrinhoComprasView {
    @EJB
    private ProdutoTemplateFacade produtoTemplateFacade;
    @EJB
    private CarrinhoComprasFacade carrinhoComprasFacade;
    
    
    
    private ProdutoDescricao produto;
    
    private int id;
    
    private String size;
    
    public CarrinhoComprasView(){
        produto = new ProdutoDescricao();
    }
    
    public String addPeca(){
        //carrinhoComprasFacade.persist(this.produto);
        return "carrinho_compras";
    }
    
    
    public ProdutoTemplate getProduto(int id){
        return produtoTemplateFacade.find(id);
    }
    
   
    
    
}
