/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ProdutoFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author rui
 */
@ManagedBean(name = "InsertNewProduto")
@RequestScoped
public class InsertNewProduto {

    @EJB
    private ProdutoFacade produtoFacade;

    public InsertNewProduto() {
        
    }

    public String listarProdutos() {
        return "listarProdutos";
    }

    public List getThisProduto() {
        return produtoFacade.getAllGuests();
    }
    

}
