package entities;

import entities.CarrinhoCompras;
import entities.Item;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-16T09:48:45")
@StaticMetamodel(ProdutoTemplate.class)
public class ProdutoTemplate_ { 

    public static volatile SingularAttribute<ProdutoTemplate, Double> preco;
    public static volatile SingularAttribute<ProdutoTemplate, String> tipo;
    public static volatile SingularAttribute<ProdutoTemplate, String> estilo;
    public static volatile SingularAttribute<ProdutoTemplate, Boolean> novidade;
    public static volatile SingularAttribute<ProdutoTemplate, Integer> id;
    public static volatile ListAttribute<ProdutoTemplate, CarrinhoCompras> carrinho;
    public static volatile ListAttribute<ProdutoTemplate, Item> items;
    public static volatile SingularAttribute<ProdutoTemplate, String> url;
    public static volatile SingularAttribute<ProdutoTemplate, String> descricao;

}