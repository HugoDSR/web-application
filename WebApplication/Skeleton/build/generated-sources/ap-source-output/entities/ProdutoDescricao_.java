package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-16T09:48:45")
@StaticMetamodel(ProdutoDescricao.class)
public class ProdutoDescricao_ { 

    public static volatile SingularAttribute<ProdutoDescricao, Double> preco;
    public static volatile SingularAttribute<ProdutoDescricao, String> tipo;
    public static volatile SingularAttribute<ProdutoDescricao, String> size;
    public static volatile SingularAttribute<ProdutoDescricao, Integer> id;
    public static volatile SingularAttribute<ProdutoDescricao, String> url;
    public static volatile SingularAttribute<ProdutoDescricao, String> descricao;

}