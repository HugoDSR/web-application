package entities;

import entities.ProdutoTemplate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-16T09:48:45")
@StaticMetamodel(Item.class)
public class Item_ { 

    public static volatile SingularAttribute<Item, String> tamanho;
    public static volatile SingularAttribute<Item, String> qrcode;
    public static volatile SingularAttribute<Item, ProdutoTemplate> idTemplate;
    public static volatile SingularAttribute<Item, Integer> id;
    public static volatile SingularAttribute<Item, Integer> barcode;

}