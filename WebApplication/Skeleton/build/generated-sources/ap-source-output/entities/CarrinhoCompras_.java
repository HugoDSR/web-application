package entities;

import entities.ProdutoTemplate;
import entities.RegistoCompra;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-16T09:48:45")
@StaticMetamodel(CarrinhoCompras.class)
public class CarrinhoCompras_ { 

    public static volatile SingularAttribute<CarrinhoCompras, RegistoCompra> registo;
    public static volatile ListAttribute<CarrinhoCompras, ProdutoTemplate> produtos;
    public static volatile SingularAttribute<CarrinhoCompras, Integer> id;
    public static volatile SingularAttribute<CarrinhoCompras, Double> precoTotal;

}