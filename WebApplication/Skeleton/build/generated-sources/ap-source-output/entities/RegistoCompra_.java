package entities;

import entities.CarrinhoCompras;
import entities.Cliente;
import entities.Funcionario;
import entities.Pagamento;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-16T09:48:46")
@StaticMetamodel(RegistoCompra.class)
public class RegistoCompra_ { 

    public static volatile SingularAttribute<RegistoCompra, Cliente> cliente;
    public static volatile SingularAttribute<RegistoCompra, String> data;
    public static volatile SingularAttribute<RegistoCompra, Integer> id;
    public static volatile SingularAttribute<RegistoCompra, CarrinhoCompras> carrinho;
    public static volatile SingularAttribute<RegistoCompra, Funcionario> funcionario;
    public static volatile SingularAttribute<RegistoCompra, Pagamento> pagamento;

}