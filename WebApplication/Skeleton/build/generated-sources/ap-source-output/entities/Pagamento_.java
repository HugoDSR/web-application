package entities;

import entities.RegistoCompra;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-16T09:48:46")
@StaticMetamodel(Pagamento.class)
public class Pagamento_ { 

    public static volatile SingularAttribute<Pagamento, Double> total;
    public static volatile SingularAttribute<Pagamento, RegistoCompra> registo;
    public static volatile SingularAttribute<Pagamento, Integer> id;

}