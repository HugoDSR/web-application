package my.presentation;

import javax.faces.bean.ManagedBean;
 
@ManagedBean
public class AdicionarView {
    
    private int id;
    private String descricao;
    private double preco;
    private String tipo;
    private String estilo;
    private boolean novidade;
    private String imagem;

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public boolean isNovidade() {
        return novidade;
    }

    public void setNovidade(boolean novidade) {
        this.novidade = novidade;
    }
            
    
}
