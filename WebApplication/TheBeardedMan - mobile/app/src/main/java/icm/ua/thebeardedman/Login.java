package icm.ua.thebeardedman;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.concurrent.ExecutionException;

import icm.ua.thebeardedman.XMLParser.XMLParserLogin;

/**
 * Created by tiagoteixeira on 19-11-2015.
 */
public class Login extends Activity {

    public final static String EXTRA_MESSAGE = ".Login";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        VideoView myVideoView = (VideoView) findViewById(R.id.videoView);

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("The Bearded Man");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.beard));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        myVideoView.requestFocus();
        progressDialog.dismiss();
        myVideoView.start();
    }

    public void login (View v){
        EditText text = (EditText) findViewById(R.id.textUser);
        EditText pass = (EditText) findViewById(R.id.textPass);
        XMLParserLogin xml = new XMLParserLogin(text.getText().toString(), pass.getText().toString());
        boolean login = false;
        try {
            login = xml.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if(login) {
            Intent intent = new Intent(Login.this, Menu.class);
            intent.putExtra(EXTRA_MESSAGE, R.layout.content_main);
            startActivity(intent);
            this.finish();
            System.exit(0);
        }
        else {
            Toast toast = Toast.makeText(this, "Login Inválido", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void fazerLogin (View v){
        Button button = (Button) findViewById(R.id.button2);
        button.setVisibility(View.GONE);
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linear);
        linearLayout.setVisibility(View.VISIBLE);
    }

    public void clearText (View v){
        EditText text;
        if(v.getId() == R.id.textUser) {
            text = (EditText) findViewById(R.id.textUser);
        } else {
            text = (EditText) findViewById(R.id.textPass);
        }
        text.setText("");
    }
}
