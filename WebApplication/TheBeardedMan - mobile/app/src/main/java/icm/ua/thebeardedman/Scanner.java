package icm.ua.thebeardedman;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import icm.ua.thebeardedman.XMLParser.XMLParserEstilo;
import icm.ua.thebeardedman.XMLParser.XMLParserIdCBarId;
import icm.ua.thebeardedman.XMLParser.XMLParserIdDescricao;
import icm.ua.thebeardedman.XMLParser.XMLParserIdEstilo;
import icm.ua.thebeardedman.XMLParser.XMLParserIdQrId;
import icm.ua.thebeardedman.XMLParser.XMLParserIdTemplateByCBar;
import icm.ua.thebeardedman.XMLParser.XMLParserIdTemplateByQR;


public class Scanner extends NavigationDrawer {

    private static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    public void rfiIdReader(View v){
        Toast toast = Toast.makeText(this, "Faça o Scan do Cartão\n", Toast.LENGTH_LONG);
        toast.show();

        Intent intent = new Intent(Scanner.this, TagViewer.class);
        intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.tag_viewer);
        startActivityForResult(intent, 0);
    }
    //product barcode mode
    public void scanBar(View v){
        try {
            //start the scanning activity from the
            // com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);

        } catch (ActivityNotFoundException ex) {
            showDialog(Scanner.this, "No Scanner Found",
                    "Download a scanner code activity?", "Yes", "No").show();
        }

    }

    //product qr code mode
    public void scanQR(View v){
        try{
            //start the scanning activity from the
            //com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);

        } catch(ActivityNotFoundException ex) {
            showDialog(Scanner.this, "No Scanner Found",
                    "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    //alert dialog for downloadDialog
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message,
                                          CharSequence buttonYes, CharSequence buttonNo){

        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    System.out.print("LOL");

                }
            }
        });

        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return downloadDialog.show();
    }

    //on ActivityResult method
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0){
            if(resultCode == RESULT_OK) {
                //get the extras that are returned from the intent
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                String idTemplate;
                String tamanho;
                String descricao = "";
                try {
                    if(true)    {//QR CODE
                        XMLParserIdTemplateByQR xmlQr = new XMLParserIdTemplateByQR(contents);
                        idTemplate = xmlQr.execute().get();
                        XMLParserIdQrId xmlqrid = new XMLParserIdQrId(contents);
                        tamanho = xmlqrid.execute().get();
                    }
                    else if(false) {//CODE BAR
                        XMLParserIdTemplateByCBar xmlBar = new XMLParserIdTemplateByCBar(contents);
                        idTemplate = xmlBar.execute().get();
                        XMLParserIdCBarId xmlcbarid = new XMLParserIdCBarId(contents);
                        tamanho = xmlcbarid.execute().get();
                    }
                XMLParserIdDescricao xmlid = new XMLParserIdDescricao(idTemplate);
                descricao = xmlid.execute().get();
                NavigationDrawer.pecas.add(descricao+" ("+tamanho+")");

                XMLParserIdEstilo xmlestilo = new XMLParserIdEstilo(idTemplate);
                String estilo = xmlestilo.execute().get();

                XMLParserEstilo xmlsug = new XMLParserEstilo(estilo);
                ArrayList<String> sugestId = xmlsug.execute().get();
                linksSugest.addAll(sugestId);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                Toast toast = Toast.makeText(this, descricao  + " adicionado com sucesso!\n", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

}
