package icm.ua.thebeardedman;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tiagoteixeira on 18-11-2015.
 */
public class NavigationDrawer extends AppCompatActivity {

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private android.view.Menu menu;
    public static List<String> pecas = new ArrayList<String>();
    public static List<String> links = new ArrayList<String>();
    public static List<String> nomes = new ArrayList<String>();
    public static List<String> linksSugest = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        int message = intent.getIntExtra(Login.EXTRA_MESSAGE, 0);
        if (message == 0)
            message = intent.getIntExtra(Menu.EXTRA_MESSAGE, 0);
        setContentView(message);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();
    }

    private void addDrawerItems() {
        String[] osArray = { "Página inicial", "Registar Produtos", "Lista de Compras" };
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(NavigationDrawer.this, Menu.class);
                    intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.content_main);
                    startActivity(intent);
                    finish();
                } else if (position == 1) {
                    Intent intent = new Intent(NavigationDrawer.this, Scanner.class);
                    intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.scan);
                    startActivity(intent);
                    finish();
                } else if (position == 2) {
                    Intent intent = new Intent(NavigationDrawer.this, MyList.class);
                    intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.shoplist);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Navegar");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        //super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        /*if(!beacon) {
            MenuItem item = menu.findItem(R.id.action_beacons);
            item.setTitle("Ativar Beacon");
            item = menu.findItem(R.id.action_logo);
            item.setIcon(R.drawable.beacoff);
        }*/
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(android.view.Menu menu) {

        MenuItem item = menu.findItem(R.id.action_beacons);
        if(!beacon) {
            item.setTitle("Ativar Beacon");
            item = menu.findItem(R.id.action_logo);
            item.setIcon(R.drawable.beacoff);
        }
        else {
            item.setTitle("Desativar Beacon");
            item = menu.findItem(R.id.action_logo);
            item.setIcon(R.drawable.beacon);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_beacons) {
            ativarDesativarBeacons(item);
            return true;
        } else if (id == R.id.action_logo) {
            return true;
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }


    private static boolean beacon = true;
    private void ativarDesativarBeacons(MenuItem item) {
        Toast toast;
        if(beacon) {
            item.setTitle("Ativar Beacon");
            item = menu.findItem(R.id.action_logo);
            item.setIcon(R.drawable.beacoff);
            toast = Toast.makeText(this, "Notificações por beacons desativadas.", Toast.LENGTH_LONG);
            beacon = false;
        } else {
            item.setTitle("Desativar Beacon");
            item = menu.findItem(R.id.action_logo);
            item.setIcon(R.drawable.beacon);
            toast = Toast.makeText(this, "Notificações por beacons ativadas.", Toast.LENGTH_LONG);
            beacon = true;
        }
        toast.show();
    }

    private void updateMenu() {
        MenuItem item = menu.findItem(R.id.action_beacons);
        if(!beacon) {
            item.setTitle("Ativar Beacon");
            //item = menu.findItem(R.id.action_logo);
            //item.setIcon(R.drawable.beacoff);
        }
        else {
            item.setTitle("Desativar Beacon");
            //item = menu.findItem(R.id.action_logo);
            //item.setIcon(R.drawable.beacon);
        }
    }


}

