package icm.ua.thebeardedman;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by tiagoteixeira on 15-12-2015.
 */
public class MyList extends NavigationDrawer {

    private int position;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        lv = (ListView) findViewById(R.id.listView);



        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pecas);

        lv.setAdapter(arrayAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View v, int _position, long id) {
                position = _position;
                showDialog(MyList.this, "Atenção",
                        "Pretende remover item da lista de compras?", "Sim", "Não", _position).show();
            }


        });
    }

    private void clickYes() {
        NavigationDrawer.pecas.remove(this.position);
        lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, NavigationDrawer.pecas));

    }

    private AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message,
                                   CharSequence buttonYes, CharSequence buttonNo, int position) {
        this.position = position;

        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int position) {
                clickYes();
            }
        });

        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int position) {

            }
        });
        return downloadDialog.show();
    }
}
