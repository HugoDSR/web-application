package icm.ua.thebeardedman.XMLParser;

import android.os.AsyncTask;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hugo on 25-11-2015.
 */
public class XMLParserIdDescricao extends AsyncTask<String, Void, String> {

    URL url;
    String id;

    public XMLParserIdDescricao(String id) {
        this.id = id;
    }

    @Override
    protected String doInBackground(String... objects) {
        // Initializing instance variables


        try {
            url = new URL("http://192.168.160.27:8080/TheBeardedManRest/api/produtotemplate");

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();

            // We will get the XML from an input stream
            xpp.setInput(getInputStream(url), "UTF-8");

        /* We will parse the XML content looking for the "<title>" tag which appears inside the "<item>" tag.
         * However, we should take in consideration that the rss feed name also is enclosed in a "<title>" tag.
         * As we know, every feed begins with these lines: "<channel><title>Feed_Name</title>...."
         * so we should skip the "<title>" tag which is a child of "<channel>" tag,
         * and take in consideration only "<title>" tag which is a child of "<item>"
         *
         * In order to achieve this, we will make use of a boolean variable.
         */
            boolean insideItem = false;
            String descricao = "";

            // Returns the type of current event: START_TAG, END_TAG, etc..
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {


                //Log.i("PARSER", "Found: " + xpp.getName());

                    // problema em ler codigos que começam da mesma forma i.e quando procuro asd, asdd tambem e lido
                    if (xpp.getName().equalsIgnoreCase("descricao")) {
                            descricao = xpp.nextText();

                    } else if (xpp.getName().equalsIgnoreCase("id")) {
                        if(xpp.nextText().equals(id)) {
                            return descricao;
                        }
                    }
                } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                    insideItem = false;
               }

                eventType = xpp.next(); //move to next element
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

}
