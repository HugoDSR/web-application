package icm.ua.thebeardedman;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import icm.ua.thebeardedman.XMLParser.XMLParserDescrNov;
import icm.ua.thebeardedman.XMLParser.XMLParserUrl;

public class Menu extends NavigationDrawer {

    public final static String EXTRA_MESSAGE = ".Menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        XMLParserUrl xmlurl = new XMLParserUrl();
        XMLParserDescrNov xmldescr = new XMLParserDescrNov();
        try {
            links = xmlurl.execute().get();
            nomes = xmldescr.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        TextView tv = (TextView) findViewById(R.id.textView5);
        tv.setText(pecas.size() + " " + getString(R.string.produtosLista));
        LinearLayout linearLayoutPai = (LinearLayout) findViewById(R.id.linearlayout);


        for (int i = 0; i < links.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT));

            ImageView imageView = new ImageView(this);
            final float scale = getResources().getDisplayMetrics().density;
            imageView.setLayoutParams(new LayoutParams((int) (250 * scale), (int) (250 * scale)));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            new DownloadImageTask(imageView).execute(links.get(i));

            TextView textView = new TextView(this);
            textView.setText(nomes.get(i));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            textView.setLayoutParams(params);
            textView.setTextColor(Color.BLACK);

            linearLayout.addView(imageView);
            linearLayout.addView(textView);

            linearLayoutPai.addView(linearLayout);
        }

        /*if(pecas.size() > 0) {

            TextView s = (TextView) findViewById(R.id.textView3);
            s.setVisibility(View.GONE);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.linearlayout);
            linearLayout2.setVisibility(View.GONE);

            LinearLayout linearLayoutPai2 = (LinearLayout) findViewById(R.id.linearlayout2);
            LinearLayout linearLayout = new LinearLayout(this);

            for (int i = 0; i < linksSugest.size(); i++) {

                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT));

                ImageView imageView = new ImageView(this);
                final float scale = getResources().getDisplayMetrics().density;
                imageView.setLayoutParams(new LayoutParams((int) (200 * scale), (int) (200 * scale)));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                XMLParserIdUrl xmlUrl = new XMLParserIdUrl(linksSugest.get(i));
                try {
                    String url = xmlUrl.execute().get();
                    new DownloadImageTask(imageView).execute(url);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                TextView textView = new TextView(this);
                XMLParserIdDesc xmlDes = new XMLParserIdDesc(linksSugest.get(i));
                try {
                    String des = xmlDes.execute().get();
                    textView.setText(des);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_HORIZONTAL;
                textView.setLayoutParams(params);
                textView.setTextColor(Color.BLACK);

                linearLayout.addView(imageView);
                linearLayout.addView(textView);

                linearLayoutPai2.addView(linearLayout);
            }
            linearLayout.setVisibility(View.VISIBLE);
            linearLayoutPai2.setVisibility(View.VISIBLE);
            TextView ss = (TextView) findViewById(R.id.textView7);
            ss.setVisibility(View.VISIBLE);
        }*/

    }

    public void registo(View v) {
        Intent intent = new Intent(this, Scanner.class);
        intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.scan);
        startActivity(intent);
        finish();
    }

    public void lista(View v) {
        Intent intent = new Intent(this, MyList.class);
        intent.putExtra(Menu.EXTRA_MESSAGE, R.layout.shoplist);
        startActivity(intent);
        finish();
    }

}
